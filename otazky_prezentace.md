# Otázky na prezentaci

**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 8 hodin |
| jak se mi to podařilo rozplánovat | Vcelku blbě dělal jsem vše poslední týden |
| design zapojení | [design zapojení](./dokumentace/schema/schema_zapojeni.png) |
| proč jsem zvolil tento design | přisel mi pěkny a efektivní |
| zapojení | [zapojení](.\dokumentace\schema\shema_elektronicke.png) |
| z jakých součástí se zapojení skládá | 1x zelená led 1x červená led 1x 100Ω rezistor 1x WS2812B pásek 1x DS18B20 čidlo teploty 1x 4.7kΩ rezistor  1x fotorezistor 1x 10kΩ rezistor  1x 16x2 LCD displej 1x i2C převodník pro LCD displej kolíková lišta PCB prototypová deska vodiče |
| realizace | [realizace](./dokumentace/fotky/zepredu_shield.jpg) |
| nápad, v jakém produktu vše propojit dohromady| žádný |
| co se mi povedlo | Video |
| co se mi nepovedlo/příště bych udělal/a jinak | Dal si trochu víc času dopředu abych předešel stresu |
| zhodnocení celé tvorby | Výsledkem práce je funkční, šikovný shield |
