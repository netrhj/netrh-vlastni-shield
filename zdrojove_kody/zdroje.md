# Zdroje kódu

## Použité knihovny třetích stran:

- **OneWire**: Knihovna pro komunikaci s OneWire zařízeními.
  - Autor: Jim Studt, Tom Pollard, Robin James, Glenn Trewitt, Jason Dangel, Guillermo Lovato, Paul Stoffregen, Scott Roberts, Bertrik Sikken, Mark Tillotson, Ken Butcher, Roger Clark, Love Nystrom
  - Odkaz: [OneWire GitHub](https://github.com/PaulStoffregen/OneWire)
- **DallasTemperature**: Knihovna pro čtení teploty z DS18B20 senzoru.
  - Autor: Miles Burton
  - Odkaz: [DallasTemperature GitHub](https://github.com/milesburton/Arduino-Temperature-Control-Library)
- **LiquidCrystal_I2C**:
  - Autor: Frank de Brabander
  - Odkaz: [LiquidCrystal_I2C GitHub](https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library)
- **FastLed**:
  - Autor: Daniel Garcia, Mark Kriegsman
  - Odkaz: [FastLed Github](https://github.com/FastLED/FastLED)
