#ifndef SHIELDSETUP_H
#define SHIELDSETUP_H

#include <ShieldLeds.h>        // Knihovna pro práci s LED diodami
#include <ShieldPhotoRes.h>    // Knihovna pro práci s fotoresistorem
#include <ShieldThermometer.h> // Knihovna pro práci s teploměrem
#include <ShieldDisplay.h>     // Knihovna pro práci s displejem
#include <ShieldRGBStrip.h>    // Knihovna pro práci s RGB páskem WS2812B

// Definice pinů pro červenou a zelenou LED diodu a vytvoření instance třídy ShieldLeds.
// - RED_LED: pin pro červenou LED
// - GREEN_LED: pin pro zelenou LED
const int RED_LED = D4;
const int GREEN_LED = D3;
ShieldLeds leds(RED_LED, GREEN_LED);  // Vytvoření objektu ShieldLeds pro práci s LED diodami.

// Definice pinu pro fotoresistor a vytvoření instance třídy ShieldPhotoRes.
// - PHOTO_RES_PIN: pin pro fotoresistor
// - Rozsah detekce světla: od 400 (tma) do 700 (světlo)
const int PHOTO_RES_PIN = A0;
ShieldPhotoRes photoRes(PHOTO_RES_PIN, 400, 700);  // Vytvoření objektu ShieldPhotoRes pro práci s fotoresistorem.

// Definice pinu pro teploměr a vytvoření instance třídy ShieldThermometer.
// - THERMOMETER_PIN: pin pro teploměr
const int THERMOMETER_PIN = D2;
ShieldThermometer thermometer(THERMOMETER_PIN);  // Vytvoření objektu ShieldThermometer pro práci s teploměrem.

// Vytvoření instance třídy ShieldDisplay pro práci s LCD displejem.
// - 0x27: I2C adresa displeje.
// - 16: počet sloupců na displeji
// - 2: počet řádků na displeji
ShieldDisplay display(0x27, 16, 2);  // Vytvoření objektu ShieldDisplay pro práci s displejem.

// Vytvoření instance třídy ShieldRGBStrip pro připojení WS2812B.
// - NUM_LEDS: počet LED diod na červenou a zelenou LED diodu
// - DATA_PIN: pin pro připojení datového signálu
const int DATA_PIN = D6;
const int NUM_LEDS = 8;
ShieldRGBStrip rgbStrip(NUM_LEDS, DATA_PIN);

// Funkce setupShield inicializuje všechny komponenty připojené k Arduino desce.
// Tato funkce by měla být zavolána v setup() pro nastavení hardwaru.
void setupShield() {
  delay(500);  // Krátké zpoždění pro stabilizaci hardwaru po resetu
  Serial.begin(115200);  // Zahájení komunikace přes sériový port s rychlostí 115200 baudů
  leds.begin();          // Inicializace LED diod
  thermometer.begin();   // Inicializace teploměru
  display.begin();       // Inicializace displeje
  rgbStrip.begin();      // Inicializace WS2812B
}

#endif