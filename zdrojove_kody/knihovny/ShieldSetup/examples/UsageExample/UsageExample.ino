#include <ShieldSetup.h>

void setup() {
  setupShield();
}

void loop() {
  //********************** leds ********************//
  // leds.lightRed();
  // delay(1000);
  
  // leds.lightGreen();
  // delay(1000);

  // leds.turnOff();
  // delay(1000);

  //******************** photoresistor ********************//
  // if (photoRes.isDark()) {
  //   leds.lightRed();
  // } else {
  //   leds.lightGreen();
  // }

  // photoRes.printValue();
  // delay(150);

  //******************** thermometer ********************//
  // thermometer.printValue();
  // delay(1000);

  //******************** display ********************//
    // display.printMessage("Hello, World", 0, "center");
    // delay(2000);

    // display.printMessage("Hello, World", 1, "right");
    // delay(2000);

    // display.clear();
    // delay(1000);

    // display.cycleText("This is a long scrolling message", 1, 300);
    // delay(2000);

    // String temp = String(thermometer.getValue(), 1);
    // String messages[] = {"Temp is " + String(thermometer.getValue(), 1), String(photoRes.getValue()), "Flash 3"};
    // display.flashArray(messages, 3, 1000);

    String multiArray[][2] = {{"Temperature: ", String(thermometer.getValue(), 1)}, {"Leds status:", leds.getStatus()},  {"Light level:", String(photoRes.getValue())}};
    display.cycleMultiArray(multiArray, 3, 1500);

    if (thermometer.getValue() < 33) {
      leds.lightGreen();

      for (int i = 0; i < 8; i++) {
        ledss[i] = CRGB::Green;
        FastLED.show();
      }
    } else {
      leds.lightRed();

      for (int i = 0; i < 8; i++) {
        ledss[i] = CRGB::Red;
        FastLED.show();
      }
    }
}
