#include <ShieldSetup.h>

void setup() {
  setupShield();
}

void loop() {

    String multiArray[][2] = {{"Temperature: ", String(thermometer.getValue(), 1)}, {"Leds status:", leds.getStatus()},  {"Light level:", String(photoRes.getValue())}};
    display.cycleMultiArray(multiArray, 3, 1500);

    if (thermometer.getValue() < 30) {
      leds.lightGreen();

      for (int i = 0; i < 8; i++) {
        ledss[i] = CRGB::Green;
        FastLED.show();
      }
    } else {
      leds.lightRed();

      for (int i = 0; i < 8; i++) {
        ledss[i] = CRGB::Red;
        FastLED.show();
      }
    }
}
