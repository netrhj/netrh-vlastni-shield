// /*
//   Cheatsheet - Přehled metod pro vlastní knihovny
// */

// /***********************
//  *  Fotorezistor (ShieldPhotoRes)
//  ***********************/
 
// // Konstruktor
// ShieldPhotoRes photoRes(int pin);

// // Metody
// void begin();            // Inicializace (pokud je potřeba)
// int getValue();          // Získání aktuální hodnoty z fotorezistoru
// void printValue();       // Vypsání hodnoty do seriové konzole
// bool isDark();           // Zjištění, zda je tma (na základě prahové hodnoty)

// /***********************
//  *  Displej (ShieldDisplay)
//  ***********************/
 
// // Konstruktor
// ShieldDisplay display(uint8_t lcd_addr, uint8_t lcd_cols, uint8_t lcd_rows);

// // Metody
// void begin();                                  // Inicializace displeje
// void printMessage(const String& message, uint8_t line, const String& align); // Vypsání zprávy na zvolenou řádku s určeným zarovnáním
// void cycleText(const String& message, uint8_t line, unsigned int delayTime); // Cyklické zobrazování textu na řádku
// void flashArray(const String arr[], uint8_t size, unsigned int delayTime);   // Postupné zobrazování textu z pole
// void cycleMultiArray(const String arr[][2], uint8_t size, unsigned int delayTime); // Cyklické zobrazování dvouřádkového textu z pole
// void clearLine(uint8_t line);                  // Vymazání obsahu zvoleného řádku
// void clear();                                  // Vymazání displeje
// void printAligned(const String& message, uint8_t line, const String& align); // Vypsání textu se zarovnáním

// /***********************
//  *  Teploměr (ShieldThermometer)
//  ***********************/
 
// // Konstruktor
// ShieldThermometer thermometer(int pin);

// // Metody
// void begin();           // Inicializace teploměru
// double getValue();      // Získání aktuální teploty ve stupních Celsia
// void printValue();      // Vypsání aktuální teploty do seriové konzole
// bool isTempRising();    // Vrací true, pokud teplota roste

// /***********************
//  *  LEDky (ShieldLeds)
//  ***********************/
 
// // Konstruktor
// ShieldLeds leds(int redPin, int greenPin);

// // Metody
// void begin();           // Inicializace pinů pro LEDky
// void turnOff();         // Vypnutí obou LEDek
// void lightGreen();      // Zapnutí zelené LEDky
// void lightRed();        // Zapnutí červené LEDky
// String getStatus();     // Získání aktuálního stavu LEDek ("Red ON", "Green ON", "OFF")

// /***********************
//  *  RGB pásek (ShieldRgbStrip)
//  ***********************/
 
// // Konstruktor
// ShieldRgbStrip rgbStrip(int numLeds, int dataPin);

// // Metody
// void begin();                                      // Inicializace RGB pásku
// void setColor(uint8_t r, uint8_t g, uint8_t b);    // Nastavení barvy všech LEDek na RGB hodnotu
// void setLedColor(int index, uint8_t r, uint8_t g, uint8_t b); // Nastavení barvy jednotlivé LEDky na RGB hodnotu
// void turnOff();                                    // Vypnutí všech LEDek
// void blinkColor(uint8_t r, uint8_t g, uint8_t b, int delayTime); // Blikání danou barvou s prodlevou
// void fadeToColor(uint8_t r, uint8_t g, uint8_t b, int steps);    // Plynulý přechod na danou barvu
// void rainbow(int delayTime);                       // Zobrazení duhového efektu
// void setBrightness(uint8_t brightness);            // Nastavení jasu LED pásku
// uint8_t getBrightness() const;                     // Získání aktuálního nastaveného jasu

