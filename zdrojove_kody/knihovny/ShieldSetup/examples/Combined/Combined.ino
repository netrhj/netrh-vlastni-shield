#include <ShieldSetup.h>

void setup() {
  setupShield();

  rgbStrip.brightness(128);
}

void loop() {
  int lightLevel = photoRes.getValue();
  double temperature = thermometer.getValue();

  // Ovládání LEDek na základě úrovně světla
  if (photoRes.isDark()) {
    leds.lightRed();  // Pokud je tma, svítí červená LED
  } else {
    leds.lightGreen(); // Pokud je světlo, svítí zelená LED
  }

  // Ovládání RGB pásku na základě teploty
  if (temperature < 20.0) {
    rgbStrip.setColor(0, 0, 255); // Modrá pro teploty pod 20 °C
  } else if (temperature >= 20.0 && temperature < 25.0) {
    rgbStrip.setColor(0, 255, 0); // Zelená pro teploty mezi 20 °C a 25 °C
  } else {
    rgbStrip.setColor(255, 0, 0); // Červená pro teploty nad 25 °C
  }

  String multiArray[][2] = {{"Temperature: ", String(thermometer.getValue(), 1)}, {"Leds status:", leds.getStatus()},  {"Light level:", String(photoRes.getValue())}};
  display.cycleMultiArray(multiArray, 3, 1500);
} 