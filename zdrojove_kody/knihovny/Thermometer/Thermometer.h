/*
  Thermometer.h - knihovna k práci s teploměrem DS18B20.
*/

#ifndef THERMOMETER_H
#define THERMOMETER_H

#include "Arduino.h"
#include "OneWire.h"
#include "DallasTemperature.h"

class Thermometer {
  public:
    Thermometer(int pin);
    void begin();
    double getValue();


  private:
    OneWire oneWire;
    DallasTemperature sensor;
    double _lastTemp = 0;
};

#endif
