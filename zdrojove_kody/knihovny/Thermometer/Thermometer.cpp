#include "Thermometer.h"


// Konstruktor třídy Thermometer, který inicializuje OneWire a DallasTemperature.
// Parametry:
// - pin: pin, ke kterému je připojen teploměr
Thermometer::Thermometer(int pin) : oneWire(pin), sensor(&oneWire) {
  // Inicializace objektů oneWire a sensor pomocí zadaného pinu.
}

// Metoda begin inicializuje senzor teploměru.
// Tuto metodu je potřeba zavolat v setup(), aby byl teploměr připraven k použití.
void Thermometer::begin() {
  sensor.begin();  // Inicializace senzoru teploměru
}

// Metoda getValue vrací aktuální hodnotu teploty v °C.
// Návratová hodnota:
// - Aktuální teplota měřená senzorem v °C.
double Thermometer::getValue() {
  sensor.requestTemperatures();  // Žádost o čtení teploty ze senzoru
  return sensor.getTempCByIndex(0);  // Vrácení teploty v °C z prvního senzoru na sběrnici
}

