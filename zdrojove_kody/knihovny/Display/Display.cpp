#include "Display.h"


// Konstruktor třídy Display, který inicializuje LCD displej s danou adresou, počtem sloupců a řádků.
// Parametry:
// - lcdAddr: adresa LCD displeje na sběrnici I2C
// - lcdCols: počet sloupců displeje
// - lcdRows: počet řádků displeje
Display::Display(uint8_t lcdAddr, uint8_t lcdCols, uint8_t lcdRows)
    : lcd(lcdAddr, lcdCols, lcdRows), _cols(lcdCols), _rows(lcdRows) {}

// Metoda begin inicializuje LCD displej a zapne podsvícení.
void Display::begin() {
    lcd.init();        // Inicializace LCD displeje
    lcd.backlight();   // Zapnutí podsvícení displeje
}

void Display::cycleMultiArray(const String arr[][2], uint8_t size, unsigned int delayTime) {
    static uint8_t currentIndex = 0;       // Aktuální index zobrazované dvojice zpráv
    static unsigned long lastUpdateTime = 0;  // Poslední čas aktualizace displeje

    unsigned long currentTime = millis();  // Aktuální čas.

    if (currentTime - lastUpdateTime >= delayTime) {  // Pokud uplynulo dost času,
        printMessage(arr[currentIndex][0], 0, "left");  // zobrazí se první zpráva na prvním řádku
        printMessage(arr[currentIndex][1], 1, "left");  // Zobrazí se druhá zpráva na druhém řádku

        lastUpdateTime = currentTime;   // Aktualizace času poslední změny
        currentIndex++;                 // Posun na další dvojici zpráv

        if (currentIndex >= size) {     // Pokud se dosáhne konce pole, začne se od začátku
            currentIndex = 0;
        }
    }
}
