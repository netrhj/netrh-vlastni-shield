#ifndef SHIELD_DISPLAY_H
#define SHIELD_DISPLAY_H

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>

class ShieldDisplay {
public:
    ShieldDisplay(uint8_t lcdAddr, uint8_t lcdCols, uint8_t lcdRows);
    void begin();
    void cycleMultiArray(const String arr[][2], uint8_t size, unsigned int delayTime);

private:
    LiquidCrystal_I2C lcd;
    uint8_t _cols;
    uint8_t _rows;
    void clearLine(uint8_t line);
    void printAligned(const String& message, uint8_t line, const String& align);
};

#endif

