/*
  PhotoRes.h - knihovna k práci s fotorezistorem.
*/

#ifndef PHOTO_RES_H
#define PHOTO_RES_H

#include "Arduino.h"

class PhotoRes {
  public:
    PhotoRes(int pin);
    int getValue();
  private:
    int _pin;
};

#endif