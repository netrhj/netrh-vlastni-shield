#include "PhotoRes.h"

// Konstruktor třídy PhotoRes, který inicializuje pin pro fotoresistor.
// Parametry:
// - pin: pin, ke kterému je připojen fotoresistor
PhotoRes::PhotoRes(int pin) {
  _pin = pin;  // Uložení pinu fotoresistoru do proměnné _pin

}

// Metoda getValue vrací aktuální hodnotu z fotoresistoru.
// Návratová hodnota:
// - Hodnota z analogového pinu (0-1023), která odpovídá intenzitě světla.
int PhotoRes::getValue() {
  return analogRead(_pin);  // Čtení hodnoty z analogového pinu
}
