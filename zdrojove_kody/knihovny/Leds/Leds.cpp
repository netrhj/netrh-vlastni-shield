#include "Leds.h"

// Konstruktor třídy Leds, který inicializuje piny pro červenou a zelenou LED diodu.
// Parametry:
// - redPin: pin, ke kterému je připojena červená LED dioda
// - greenPin: pin, ke kterému je připojena zelená LED dioda
Leds::Leds(int redPin, int greenPin) {
  _redPin = redPin;      // Uložení pinu červené LED do proměnné _redPin
  _greenPin = greenPin;  // Uložení pinu zelené LED do proměnné _greenPin
}

// Metoda begin inicializuje piny pro LED diody a vypne obě LED.
// Tato metoda musí být zavolána v setup() před použitím ostatních funkcí.
void Leds::begin() {
  pinMode(_redPin, OUTPUT);    // Nastavení pinu červené LED jako výstup
  pinMode(_greenPin, OUTPUT);  // Nastavení pinu zelené LED jako výstup
  turnOff();                   // Vypnutí obou LED
}

// Metoda turnOff vypne obě LED diody.
void Leds::turnOff() {
  digitalWrite(_redPin, LOW);    // Vypnutí červené LED
  digitalWrite(_greenPin, LOW);  // Vypnutí zelené LED
}

// Metoda lightGreen rozsvítí zelenou LED diodu a vypne červenou.
void Leds::lightGreen() {
  digitalWrite(_redPin, LOW);    // Vypnutí červené LED
  digitalWrite(_greenPin, HIGH); // Rozsvícení zelené LED
}

// Metoda lightRed rozsvítí červenou LED diodu a vypne zelenou.
void Leds::lightRed() {
  digitalWrite(_redPin, HIGH);   // Rozsvícení červené LED
  digitalWrite(_greenPin, LOW);  // Vypnutí zelené LED
}


