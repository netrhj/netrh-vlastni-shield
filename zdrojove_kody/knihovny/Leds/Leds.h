/*
  Leds.h - knihovna k ovládání stavu 2 led na shieldu
*/

#ifndef LEDS_H
#define LEDS_H

#include "Arduino.h"

class Leds {
  public:
    Leds(int redPin, int greenPin);
    void begin();
    void turnOff();
    void lightGreen();
    void lightRed();
  
  private:
    int _redPin;
    int _greenPin;
};

#endif