#ifndef RGB_STRIP_H
#define RGB_STRIP_H

#include <FastLED.h>

class RgbStrip {
  public:
    RgbStrip(int numLeds, int dataPin);
    void begin();
    void setColor(uint8_t r, uint8_t g, uint8_t b);
    void setLedColor(int index, uint8_t r, uint8_t g, uint8_t b);
    void turnOff();


  private:
    int _numLeds;
    int _dataPin;
    uint8_t _brightness;
    CRGB* _leds;
};

#endif
