#include "RgbStrip.h"

// Konstruktor pro RgbStrip třídu.
// Parametry:
// - numLeds: počet LED diod na pásku
// - dataPin: pin, na který je připojen datový vstup LED pásku
RgbStrip::RgbStrip(int numLeds, int dataPin) 
  : _numLeds(numLeds), _dataPin(dataPin), _brightness(255) {
  _leds = new CRGB[_numLeds]; // Dynamické alokování paměti pro LED pole
}

// Metoda pro inicializaci LED pásku.
// Tato metoda přidá pás LED diod do FastLED systému a nastaví výchozí jas.
void RgbStrip::begin() {
  FastLED.addLeds<NEOPIXEL, _dataPin>(_leds, _numLeds); // Inicializace LED pásku
  FastLED.setBrightness(_brightness); // Nastavení jasu LED pásku
  turnOff(); // Vypnutí všech LED diod po inicializaci
}

// Metoda pro nastavení barvy všech LED diod na pásku.
// Parametry:
// - r: červená složka barvy (0-255)
// - g: zelená složka barvy (0-255)
// - b: modrá složka barvy (0-255)
void RgbStrip::setColor(uint8_t r, uint8_t g, uint8_t b) {
  for (int i = 0; i < _numLeds; i++) {
    _leds[i] = CRGB(r, g, b); // Nastavení barvy pro každou LED diodu
  }
  FastLED.show(); // Promítnutí změn
}

// Metoda na nastavení barvy jednotlivé LED diody.
// Parametry:
// - index: Index LED diody (0 až _numLeds - 1)
// - r: červená složka barvy (0-255)
// - g: zelená složka barvy (0-255)
// - b: modrá složka barvy (0-255)
void RgbStrip::setLedColor(int index, uint8_t r, uint8_t g, uint8_t b) {
  if (index >= 0 && index < _numLeds) {
    _leds[index] = CRGB(r, g, b); // Nastavení barvy pro specifickou LED diodu
    FastLED.show(); // Promítnutí změn
  }
}

// Metoda pro vypnutí všech LED diod na pásku.
void RgbStrip::turnOff() {
  setColor(0, 0, 0); // Nastavení všech LED diod na černou (vypnutí)
}

