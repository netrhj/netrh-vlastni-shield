# Otázky na prezentaci

**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 6 hodin |
| jak se mi to podařilo rozplánovat | Nejprve jsem si vytvořil schéma zapojení ve Fritzingu. Následně jsem si každou součástku společně s knihovnami vyzkoušel na breadboardu a následně vše připájel na PCB desku. |
| design zapojení | [design zapojení](./dokumentace/schema/schema_zapojeni.png) |
| proč jsem zvolil tento design | úspora místa |
| zapojení | [zapojení](.\dokumentace\schema\shema_elektronicke.png) |
| z jakých součástí se zapojení skládá | 1x zelená led <br> 1x červená led <br> 1x 100Ω rezistor <br> 1x WS2812B pásek <br> 1x DS18B20 čidlo teploty <br> 1x 4.7kΩ rezistor <br> 1x fotorezistor <br> 1x 10kΩ rezistor <br> 1x 16x2 LCD displej <br> 1x i2C převodník pro LCD displej <br> kolíková lišta <br> PCB prototypová deska <br> vodiče |
| realizace | [realizace](./dokumentace/fotky/kompletni.jpg) |
| nápad, v jakém produktu vše propojit dohromady| |
| co se mi povedlo | Úspora prostoru díky dobře umístěnému lcd displeji. |
| co se mi nepovedlo/příště bych udělal/a jinak | Umístění některých součástek, které zabírá více prostoru, než je nutné. |
| zhodnocení celé tvorby | Výsledkem práce je funkční shield společně s vlastními knihovnami pro každou součástku. |
